<?php
/**
 * Template Name: Front Page
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package studio_mega
 */

get_header(); ?>


			<?php
			while ( have_posts() ) : the_post(); ?>
			<!--<header id="top-header">
				<div class="contain">
					<a href="#studio" class="v-align">Studio</a>
					<img class="hero-logo" alt="Studio Mega Portland Oregon Creative Team" src="/wp-content/themes/studio-mega/images/studio-mega-logo-black.svg">
					<a href="#" class="v-align">The Work</a>
				</div>
			</header>-->

				<section class ="mega-section top-section">
					<div class="contain">
						<a href="#studio" class="v-align">Studio</a>
						
							<img class="hero-logo" alt="Studio Mega Portland Oregon Creative Team" src="/wp-content/themes/studio-mega/images/studio-mega-logo.svg">
						
						<a href="#" class="v-align">The Work</a>
					</div>
				</section>

				<section id="studio" class="mega-section intro-section">
					
					<div class="contain">
						<div class="slash"></div>
						<div class="intro-top">
							<h1>Studio Mega is a dynamic creative team based in Portland, Oregon.</h1>
						</div>
						<div class="intro-bottom">
						<p>We make things that get your attention. For partners big and small. Startups or industry leaders. Online and offline. Starting from scratch or recalibrating. We sweat the details and push beyond trends to make work that has purpose. Creative that has real impact for our partners.</p>
						</div>
					</div>
				</section>

				<section class="mega-section about-section">
					<div class="about-left">
						<p>Our work is led by a team of directors with decades of experience both in-house and on the agency side.</p>
						<p>We've led teams at some of the world's most celebrated companies for some of today's most influential brands.</p>
					</div>
					<div class="about-right">
						<div class="mega-man" style="background-image: url(/wp-content/themes/studio-mega/images/color_1.jpg);">

							
							<div class="content">
								<div class="name-tag-contain">
									<h2 class="name-tag">Josh Beyer</h1><br>
									<h3 class="title-tag">Business Director</h3>
								</div>
							</div>
						

						</div>
						<div class="mega-man" style="background-image: url(/wp-content/themes/studio-mega/images/color_2.jpg);">
							<div class="content">
								<div class="name-tag-contain">
									<h2 class="name-tag">David Jacobson</h1><br>
									<h3 class="title-tag">Creative Director</h3>
								</div>
							</div>
						</div>
						<div class="mega-man" style="background-image: url(/wp-content/themes/studio-mega/images/color_3.jpg);">
							<div class="content">
								<div class="name-tag-contain">
									<h2 class="name-tag">Trent Mitchell</h1><br>
									<h3 class="title-tag">Creative Director</h3>
								</div>
							</div>
						</div>
						<div class="mega-man" style="background-image: url(/wp-content/themes/studio-mega/images/color_4.jpg);">
							<div class="content">
								<div class="name-tag-contain">
									<h2 class="name-tag">Sean Barrett</h1><br>
									<h3 class="title-tag">Creative Director</h3>
								</div>
							</div>
						</div>
					</div>
				</section>

				<section class="mega-section capabilities-section">
					<div class="contain">
						<h1>What we do (really well).</h1>
						<div class="frame">
							<div class="bit-3">
								<h2>Identity</h2>
								<h3>Setting a Foundation</h3>
								<ul>
									<li>Identity Systems</li>
									<li>Visual Language</li>
									<li>Brand Platforms</li>
									<li>Brand Guidelines</li>
								</ul>
							</div>
							<div class="bit-3">
								<h2>Content</h2>
								<h3>Creating Originals</h3>
								<ul>
									<li>Messaging</li>
									<li>Photography</li>
									<li>Video</li>
									<li>Illustration</li>
								</ul>
							</div>
							<div class="bit-3">
							<h2>Activation</h2>
								<h3>Sharing the Experience</h3>
								<ul>
									<li>Print</li>
									<li>Environmental / Event</li>
									<li>Digital</li>
									<li>Packaging</li>
								</ul>
							</div>

						</div>
					</div>
				</section>

				<section class="mega-section partners-section">
					<div class="contain">
						
						<h1>The people who<br>like working with us.</h1>
						
							<ul>
								<li>Nike</li>
								<li>Jordan</li>
								<li>Herman Miller</li>
								<li>Microsoft</li>
								<li>Provenance Hotels</li>
								<li>Pair of Thieves</li>
								<li>Stop, Breathe & Think</li>
							</ul>
					
					</div>
				</section>

				<section class="mega-section content-section">
					<div class="contain">
						<div class="slash"></div>
						<div class="contact-top">
							<h3>To work with us, request a portfolio,<br>or join our team, email us at:<br><a class="hilite" href="mailto:yo@studiomega.com">yo@studiomega.com</a></h3>
							<h3><a href="http://www.instagram.com">Instagram</a><br>
							<a href="http://www.twitter.com">Twitter</a><br>
							<a href="http://www.linkedin.com">Linkedin</a></h3>
							<p class="location">Studio Mega<br>939 SE Alder St, Unit 1<br>Portland, Oregon 97214</p>
							<h1 class="hup">Hit us up.</h1>
					</div>
				</section>



			

			<?php 
			endwhile; // End of the loop.
			?>


<?php
get_footer();
