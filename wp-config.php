<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'fuzzllc_studio_m');

/** MySQL database username */
define('DB_USER', 'fuzzllc_studio_m');

/** MySQL database password */
define('DB_PASSWORD', 'crgNdr0d');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'BTptFKj0ox3CVb0jSVDi7dfMqX3JcBnHFipk9eNGv5S1FnhdehLnhriMhzmC7KNS');
define('SECURE_AUTH_KEY', '8QIK5x4EJ7xuxZbRgtjFPJySaQ86IWIeC7bPYSEpFNehg10AJoCk7vEQrz3Gd9mq');
define('LOGGED_IN_KEY', '1axqCJBwwuws7e1HyAV5Oz5aQcMYz9nr2pKJbV2mVu6w7KDpGfSi9M5dZnQojSFT');
define('NONCE_KEY', 'fb3vVZe6ymIqTFdSop1ntXRhXk3Mt4RmoQqhthKFzM7Q2pagJwI3uB5cBmdlH5e1');
define('AUTH_SALT', 'x1wX1jlD1ilCs9GhN0dGD2g8U5yQ31KouBvirgqtnoXyAk66GF3L961JflsoMm6u');
define('SECURE_AUTH_SALT', 'imPNSgWV1ePEhUdsW5WZU6IYQetQayXuQ0pA4H2a80cCkI1BLRp7DLoHFtQnhaA6');
define('LOGGED_IN_SALT', 'AAMeWimpVzp4qEYd6wqIQ4g3fqogwpmmsNeMoMNmfdhGrpFXVpVLjUWM7Cddumik');
define('NONCE_SALT', '8vpSlVny1LGLpqFHhmBMdBESbGIDV28I0xbw2osQXU6L1OH5b3c9ILI4rPHPUnj3');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
